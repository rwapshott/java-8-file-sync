package uk.co.gencoreoperative.funcprog.filesync;

import org.junit.After;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

import static org.junit.Assert.*;

public class MessageFunctionalTest {
    private final List<Message> toShutdown = new ArrayList<>();
    private final Supplier<String> localhost = () -> {
        try {
            return InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException ignored) {
            throw new IllegalStateException();
        }
    };

    private static final int PORT = 12345;

    @After
    public void teardown() {
        toShutdown.forEach(Message::shutdown);
        toShutdown.clear();
    }

    @Test
    public void shouldRespondWithResponseToRequest() {
        Message receiver = Message.newMessageBuilder()
                .onPort(PORT)
                .receive("PING"::equals)
                .respondWith(request -> new ByteArrayInputStream("PONG".getBytes()))
                .build();
        toShutdown.add(receiver);

        Message sender = Message.newMessageBuilder().build();
        sender.message(localhost.get(), PORT, "PING", stream -> {
            Optional<String> first = new BufferedReader(new InputStreamReader(stream)).lines().findFirst();
            assertTrue(first.isPresent());
            assertEquals(first.get(), "PONG");
        });
        toShutdown.add(sender);
    }

    /**
     * If the receiver responds with an empty message, the sender should not have to process this empty
     * message.
     */
    @Test
    public void shouldNotPassOnEmptyMessageToCaller() {
        // Responds with empty string.
        Message receiver = Message.newMessageBuilder()
                .onPort(12345)
                .receive(message -> true)
                .respondWith(message -> new ByteArrayInputStream("".getBytes())).build();
        toShutdown.add(receiver);

        Message sender = Message.newMessageBuilder().build();
        toShutdown.add(sender);

        sender.messageAsString(localhost.get(), 12345, "PING", response -> fail());

    }
}