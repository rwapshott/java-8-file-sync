package uk.co.gencoreoperative.funcprog.filesync;

import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

public class FolderEntryTest {

    private static final Path CWD = FolderEntry.getCWD();

    @Test
    public void shouldNotListFile() throws IOException {
        File abc = File.createTempFile("abc", ".def");
        assertTrue(abc.exists() && abc.isFile());
        Optional<FolderEntry> optional = FolderEntry.list(abc.toPath());
        assertFalse(optional.isPresent());
    }

    @Test
    public void shouldContainARelativePath() {
        FolderEntry entry = FolderEntry.list(CWD).get();
        assertEquals(entry.getRelativePath(), "");
    }

    @Test
    public void shouldGenerateAListing() {
        Optional<FolderEntry> entry = FolderEntry.list(CWD);
        assertTrue(entry.isPresent());
    }

    @Test
    public void shouldConvertToJSON() {
        Optional<FolderEntry> optional = FolderEntry.list(CWD);
        optional.ifPresent(entry -> assertFalse(entry.toJson().isEmpty()));
    }

    @Test
    public void shouldHaveDifferentCheckSumsForDifferentPaths() {
        FolderEntry entry1 = FolderEntry.list(new File("/").toPath()).get();
        FolderEntry entry2 = FolderEntry.list(FolderEntry.getCWD()).get();
        assertNotEquals(entry1.checksum(), entry2.checksum());
    }

    @Test
    public void shouldIndicateNewFileToCopy() {
        FolderEntry source = makeTestFolderEntry();
        FolderEntry target = makeTestFolderEntry();
        target.files.remove("badger");
        List<String> newFiles = target.getModifiedOrNewFiles(source);
        assertTrue(newFiles.contains("badger"));
    }

    @Test
    public void shouldIndicateModifiedFileToCopy() {
        FolderEntry source = makeTestFolderEntry();
        FolderEntry target = makeTestFolderEntry();
        target.files.put("badger", 1111L);
        List<String> newFiles = target.getModifiedOrNewFiles(source);
        assertTrue(newFiles.contains("badger"));
    }

    @Test
    public void shouldIndicateFileToDelete() {
        FolderEntry source = makeTestFolderEntry();
        FolderEntry target = makeTestFolderEntry();
        source.files.remove("badger");
        List<String> newFiles = target.getDeletedFiles(source);
        assertTrue(newFiles.contains("badger"));
    }

    @Test (expected = IllegalStateException.class)
    public void shouldNotCompareFolderEntriesForDifferentPaths() {
        FolderEntry source = makeTestFolderEntry();
        source.relativePath = "a/b";
        FolderEntry target = makeTestFolderEntry();
        target.relativePath = "b/c";
        target.getModifiedOrNewFiles(source);
    }

    @Test
    public void shouldIndicateEntirePathOfFileToCopy() {
        FolderEntry source = makeTestFolderEntry();
        source.relativePath = "a/b";
        FolderEntry target = makeTestFolderEntry();
        target.relativePath = "a/b";
        source.files.remove("badger");
        List<String> newFiles = target.getDeletedFiles(source);
        assertTrue(newFiles.contains("a/b/badger"));
    }

    @Test
    public void shouldReturnAllFilesInEntry() {
        FolderEntry source = makeTestFolderEntry();
        source.relativePath = "a";
        List<String> files = source.getAllFiles();
        assertTrue(files.contains("a/badger"));
        assertTrue(files.contains("a/weasel"));
    }

    private static FolderEntry makeTestFolderEntry() {
        FolderEntry entry = new FolderEntry();
        entry.relativePath = "";
        entry.files.put("badger", 1234L);
        entry.files.put("weasel", 4321L);
        return entry;
    }
}