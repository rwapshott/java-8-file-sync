package uk.co.gencoreoperative.funcprog.filesync;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class UtilsTest {
    @Test
    public void shouldParseInteger() {
        int value = Utils.parseInt("1").get();
        assertEquals(value, 1);
    }

    @Test
    public void shouldParseEmptyInteger() {
        assertFalse(Utils.parseInt("").isPresent());
    }
}