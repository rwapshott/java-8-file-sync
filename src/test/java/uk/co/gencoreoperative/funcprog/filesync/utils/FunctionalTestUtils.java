package uk.co.gencoreoperative.funcprog.filesync.utils;

import uk.co.gencoreoperative.fileutils.StreamUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.List;

import static java.text.MessageFormat.format;
import static org.junit.Assert.assertTrue;

/**
 * A collection of utilities to help functional testing.
 */
public class FunctionalTestUtils {
    private static final List<String> names = StreamUtils.readLinesToList(FunctionalTestUtils.class.getResourceAsStream("/word-list"));
    private static final SecureRandom random = new SecureRandom();

    /**
     * @return The TMP folder for the file system, required for operations in this class.
     */
    public static File getTmpFolder() {
        String tmpDirPath = System.getProperty("java.io.tmpdir");
        return new File(tmpDirPath);
    }

    /**
     * Creates a random file or folder in the given folder.
     * @param folder Parent folder which must exist.
     * @return A file or folder. If a file is created, then the file will have a random contents.
     */
    public static File makeRandom(File folder) {
        File fileOrFolder = null;
        while (fileOrFolder == null || fileOrFolder.exists()) {
            fileOrFolder = new File(folder, randomName());
        }

        if (decision()) {
            makeFolder(fileOrFolder);
        } else {
            try (BufferedWriter out = new BufferedWriter(new FileWriter(fileOrFolder))){
                random.ints(randomAtLeast(10, 100))
                        .mapToObj(String::valueOf)
                        .forEach(s -> writeSilently(out, s));
                out.flush();
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
        return fileOrFolder;
    }

    public static void makeFolder(File folder) {
        assertTrue(format("Created folder: {0}", folder.getPath()), folder.mkdir());
    }

    private static void writeSilently(BufferedWriter writer, String value) {
        try {
            writer.write(value);
        } catch (IOException ignored) {}
    }

    public static boolean decision() {
        return random.nextBoolean();
    }

    public static String random() {
        return String.valueOf(random.nextInt());
    }

    public static String randomName() {
        return names.get(random.nextInt(names.size()));
    }

    public static int randomAtLeast(int min, int max) {
        return Math.max(min, random.nextInt(max));
    }
}
