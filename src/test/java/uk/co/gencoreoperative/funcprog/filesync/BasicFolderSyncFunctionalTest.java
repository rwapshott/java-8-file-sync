package uk.co.gencoreoperative.funcprog.filesync;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import static uk.co.gencoreoperative.funcprog.filesync.utils.FunctionalTestUtils.*;
import static java.text.MessageFormat.format;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * A series of tests to demonstrate functionality of the {@link FolderSync} in a
 * meaningful way on the local machine.
 *
 * Test sequence will be to create two folders with random contents, and then
 * perform a sync between the two folders to verify that {@link FolderSync} can
 * operate as both sender and receiver in a peer to peer test.
 */
public class BasicFolderSyncFunctionalTest {
    private File sourceFolder;
    private File targetFolder;
    private String localhost;

    @Before
    public void setup() throws UnknownHostException {
        localhost = InetAddress.getLocalHost().getHostName();

        // Create two folders
        sourceFolder = new File(getTmpFolder(), "source");
        makeFolder(sourceFolder);
        populateFolder(sourceFolder, 0, randomAtLeast(1, 8));

        targetFolder = new File(getTmpFolder(), "target");
        makeFolder(targetFolder);
        populateFolder(targetFolder, 0, randomAtLeast(1, 8));
    }

    @After
    public void tearDown() throws IOException {
        Consumer<Path> print = path -> System.out.println(format(
                "{0}:{1}",
                path.toFile().isDirectory() ? "D" : "F",
                path.toString()));
        Consumer<Path> delete = path -> assertTrue(path.toFile().delete());

        Function<File, List<Path>> reverseWalk = folder -> {
            List<Path> paths = Utils.walkSilently(folder.toPath()).collect(Collectors.toList());
            Collections.reverse(paths);
            return paths;
        };

        reverseWalk.apply(sourceFolder).forEach(print.andThen(delete));
        reverseWalk.apply(targetFolder).forEach(print.andThen(delete));
    }

    /**
     * Given a folder, populate it with some random contents.
     * @param parent Non null parent folder.
     * @param currentDepth A depth used during recursion to stop before we go too deep.
     */
    private static void populateFolder(File parent, int currentDepth, int maxDepth) {
        if (currentDepth >= maxDepth) return;
        int total = randomAtLeast(1, 10);

        for (int ii = 0; ii < total; ii++) {
            File file = makeRandom(parent);
            if (file.isDirectory()) {
                populateFolder(file, ++currentDepth, maxDepth);
            }
        }
    }

    @Test
    public void shouldSyncFilesFromSourceToTarget() throws UnknownHostException {
        int port = 12345;

        // Startup FolderSync in source folder
        FolderSync source = new FolderSync(sourceFolder);
        source.listen(port);

        // Startup FolderSync in target folder
        FolderSync target = new FolderSync(targetFolder);

        // Trigger compare
        target.sync(localhost, port);

        // Verify by sorted file name walk that source and target folders are not the same.
        List<Path> sourceFiles = Utils.walkSilently(sourceFolder.toPath())
                .filter(path -> path.toFile().isFile())
                .collect(Collectors.toList());
        List<Path> targetFiles = Utils.walkSilently(targetFolder.toPath())
                .filter(path -> path.toFile().isFile())
                .collect(Collectors.toList());
        assertEquals("source and target files should equal", sourceFiles.size(), targetFiles.size());

        // Verify by sorted file walk that source and target folder are identical.
        source.shutdown();
        target.shutdown();
    }
}
