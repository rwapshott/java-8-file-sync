package uk.co.gencoreoperative.funcprog.filesync.utils;

import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class FunctionalTestUtilsTest {
    @Test
    public void shouldCreateRandomFileOrFolder() {
        File file = FunctionalTestUtils.makeRandom(FunctionalTestUtils.getTmpFolder());
        assertTrue(file != null);
        if (file.isFile()) {
            assertTrue(file.length() > 0);
        }
        assertTrue(file.delete());
    }

    @Test
    public void shouldHaveATmpFolder() {
        assertTrue(FunctionalTestUtils.getTmpFolder().exists());
    }

    @Test
    public void shouldReadRandomName() {
        assertNotNull(FunctionalTestUtils.randomName());
    }
}
