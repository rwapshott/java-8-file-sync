package uk.co.gencoreoperative.funcprog.filesync;


import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Sends and receives messages to and from peers. Intended for a simple
 * communication protocol where a sender requests something, and the
 * receiver responds with something.
 *
 * Those somethings are strings, and everything is newline separated.
 *
 * This class will automatically handle the details of managing the sockets
 * for the communication.
 */
public class Message {
    private final Map<Predicate<String>, Function<String, InputStream>> receiverMap;
    private final ExecutorService service;

    private Message(int port, Map<Predicate<String>, Function<String, InputStream>> receivers) {
        this.receiverMap = receivers;
        service = Executors.newSingleThreadExecutor();
        Runtime.getRuntime().addShutdownHook(new Thread(service::shutdownNow));

        service.execute(() -> {
            try (final ServerSocket socket = new ServerSocket(port)) {
                socket.setSoTimeout(100);
                while (!Thread.interrupted()) {
                    try (final Socket acceptedSocket = socket.accept()) {
                        selectReceiver(acceptedSocket).ifPresent(stream -> streamResponse(acceptedSocket, stream));
                    } catch (IOException ignored) {
                        // Socket can timeout on accept, continue in this case
                    }
                }
            } catch (IOException e) {
                System.err.println("Failed to create socket on: " + port);
            }
        });
    }

    /**
     * Send a message to another peer.
     *
     * The message will be a single string which has meaning in the protocol
     * and the response will be a stream of things the receiver sent in response.
     *
     * @param hostname Null assumes localhost.
     * @param port Non negative port number to connect to.
     * @param send Null assumes empty.
     * @param read Non null response which might be empty. Closing the reader will close
     *             the connection, which will happen automatically.
     */
    public void message(String hostname, int port, String send, Consumer<InputStream> read) {
        try {
            Socket socket = new Socket(hostname == null ? "localhost" : hostname, port);
            // Send the request
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            writer.write(send);
            writer.newLine();
            writer.flush();
            // Read the response
            read.accept(socket.getInputStream());
            // Close the socket
            socket.close();
        } catch (IOException ignored) {}
    }

    /**
     * Send a message to another peer in string format.
     *
     * A decorator function for {@link #message(String, int, String, Consumer)} where the
     * stream is interpreted as a String for the caller.
     *
     * @param hostname {@inheritDoc}
     * @param port {@inheritDoc}
     * @param send {@inheritDoc}
     * @param receive Non null response which will be in string format.
     */
    public void messageAsString(String hostname, int port, String send, Consumer<String> receive) {
        message(hostname, port, send, stream -> {
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(stream))) {
                String r = "";
                String data = "";
                while (data != null) {
                    data = reader.readLine();
                    if (data == null) continue;
                    r += data + "\n";
                }
                if (!r.isEmpty()) receive.accept(r);
            } catch (IOException ignored) {}
        });
    }

    /**
     * Handles a socket by reading a line from the input, and selecting a receiver
     * to process the line.
     *
     * @param socket Non null
     */
    private Optional<InputStream> selectReceiver(Socket socket) {
        try { // Note closing Input/Output stream will close the socket connection
            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String line = reader.readLine();
//            System.out.println(format("Read: {0}", line));
            for (Predicate<String> receiver : receiverMap.keySet()) {
                if (receiver.test(line)) {
                    return Optional.of(receiverMap.get(receiver).apply(line));
                }
            }
        } catch (IOException e) {}
        return Optional.empty();
    }

    /**
     * Given an InputStream, draw from this stream and pipe the contents to the socket output
     * steam.
     *
     * @param socket Non null socket to communicate on.
     * @param stream Non null stream to draw from.
     */
    private void streamResponse(Socket socket, InputStream stream) {
        // Note cannot use auto-close with socket based streams as closing stream will close socket
        try (BufferedInputStream in = new BufferedInputStream(stream)){
            BufferedOutputStream out = new BufferedOutputStream(socket.getOutputStream());
            byte[] buf = new byte[8000];
            int read = 0;
            while (read != -1) {
                read = in.read(buf);
                if (read == -1) continue;
                out.write(buf, 0, read);
            }
            out.flush();
        } catch (IOException ignored) {}
    }

    /**
     * The {@link Message} will close its resources when the application closes automatically
     * but if required, can shutdown earlier to free resources.
     */
    public void shutdown() {
        service.shutdownNow();
        try {
            service.awaitTermination(1, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * @return a new builder which will aid in assembling a Message instance.
     */
    public static MessageBuilder newMessageBuilder() {
        return new MessageBuilder();
    }

    public static class MessageBuilder {
        private int port;
        private final Map<Predicate<String>, Function<String, InputStream>> receiversMap = new HashMap<>();

        private MessageBuilder() {}

        /**
         * Should the {@link Message} be required to respond to incoming requests
         * then specify the port number to listen on.
         *
         * @param port Non zero port.
         * @return {@link RequestBuilder} to handle the subsequent request/response pairs.
         */
        public RequestBuilder onPort(int port) {
            this.port = port;
            return new RequestBuilder(this);
        }

        public Message build() {
            if (port == -1) {
                throw new IllegalStateException("Port must be assigned.");
            }
            return new Message(port, receiversMap);
        }
    }

    public static class RequestBuilder {
        private MessageBuilder messageBuilder;

        private RequestBuilder(MessageBuilder messageBuilder) {
            this.messageBuilder= messageBuilder;
        }

        /**
         * Define a request/response pair with the builder. If the peer receives the
         * request described in the predicate then it will trigger a response for the
         * caller.
         *
         * @param request The request which the {@link Message} receives.
         * @return Predicate that calculates if this request can be responded to.
         */
        public ResponseBuilder receive(Predicate<String> request) {
            return new ResponseBuilder(messageBuilder, this, request);
        }

        public Message build() {
            return messageBuilder.build();
        }
    }

    public static class ResponseBuilder {

        private MessageBuilder messageBuilder;
        private RequestBuilder requestBuilder;
        private Predicate<String> request;
        private ResponseBuilder(MessageBuilder messageBuilder, RequestBuilder requestBuilder, Predicate<String> request) {
            this.messageBuilder = messageBuilder;
            this.requestBuilder = requestBuilder;
            this.request = request;
        }
        /**
         * The response in the request/response pair. If the caller provides a request
         * that matches, then this method captures a function which will generate
         * the response to the caller.
         *
         * @param response The request which will generate the response
         * @return A stream of responses based on the request made
         */
        public RequestBuilder respondWith(Function<String, InputStream> response) {
            messageBuilder.receiversMap.put(request, response);
            return requestBuilder;
        }
    }
}
