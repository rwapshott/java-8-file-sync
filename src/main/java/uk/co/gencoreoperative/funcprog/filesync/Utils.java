package uk.co.gencoreoperative.funcprog.filesync;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;
import java.util.stream.Stream;

public class Utils {
    /**
     * Iterate over files and folders in a file system without making a noise.
     * @param folder Non null folder.
     * @return Stream of each available path.
     */
    public static Stream<Path> walkSilently(Path folder) {
        try {
            return Files.walk(folder);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * Iterate over only folders in a file system without making a noise.
     * @param folder Non null folder.
     * @return Stream of each available path.
     */
    public static Stream<Path> walkFoldersSilently(Path folder) {
        return walkSilently(folder).filter(path -> path.toFile().isDirectory());
    }

    /**
     * Open a stream without making a noise.
     * @param path Non null path to a file.
     * @return InputStream of the contents.
     */
    public static InputStream openSilently(File path) {
        try {
            return new FileInputStream(path);
        } catch (FileNotFoundException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * Parse an Integer and if the parsing failed indicate using {@link Optional}
     * @param value Possibly null string.
     * @return Optional which may or may not contain a number.
     */
    public static Optional<Integer> parseInt(String value) {
        try {
            return Optional.of(Integer.valueOf(value));
        } catch (NumberFormatException e) {
            return Optional.empty();
        }
    }
}
