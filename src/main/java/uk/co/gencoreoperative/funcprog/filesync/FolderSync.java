package uk.co.gencoreoperative.funcprog.filesync;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.function.Consumer;

import static java.text.MessageFormat.format;
import static uk.co.gencoreoperative.fileutils.StreamUtils.copyStream;

/**
 * A utility which will scan the local file system and build up a map of the
 * folders found. It will then connect to a remote system which has done the
 * same and request the checksums for the folders found.
 *
 * If any of the folders do not match then subsequent requests will be made
 * for the specific paths and their contents will be returned.
 */
public class FolderSync {
    public static final Gson GSON = new Gson();
    private final Map<Integer, FolderEntry> entryMap = new HashMap<>();
    private Message message;
    private final File root;

    /**
     * Starts the FolderSync in the current working directory the application was started in.
     */
    public FolderSync() {
        this(FolderEntry.getCWD().toFile());
    }

    /**
     * Start the {@link FolderSync} in a specific folder.
     *
     * @param root Non null folder to start the server in.
     */
    public FolderSync(final File root) {
        this.root = root;
        System.out.println(format("Scanning folder: {0}", root.getPath()));
        Utils.walkFoldersSilently(root.toPath())
                .forEach(path -> FolderEntry.list(root.toPath(), path)
                .ifPresent(entry -> entryMap.put(entry.checksum(), entry)));
        System.out.println(format("Scan complete: {0} checksums computed", entryMap.size()));
    }

    /**
     * Signal the application to listen on the specific port for requests coming
     * from a client.
     *
     * @param port Non zero port number to listen on.
     */
    public void listen(int port) {
        message = Message.newMessageBuilder()
                .onPort(port)
                // Protocol: responding to checksum request with Data Stream
                .receive("checksum"::equals)
                .respondWith(input -> new ByteArrayInputStream(GSON.toJson(entryMap.keySet()).getBytes()))
                // Protocol: responding to individual folder checksums
                .receive(request -> Utils.parseInt(request).isPresent())
                .respondWith(request -> new ByteArrayInputStream(entryMap.get(Integer.valueOf(request)).toJson().getBytes()))
                // Protocol: For a file path, stream the contents.
                .receive(request -> FolderEntry.getFullPath(root, request).isFile())
                .respondWith(request -> Utils.openSilently(FolderEntry.getFullPath(root, request)))
                .build();
    }

    /**
     * Initalise a synchronization against the remote client.
     *
     * @param hostname
     * @param port
     */
    @SuppressWarnings("unchecked")
    public void sync(String hostname, int port) {
        message = Message.newMessageBuilder().build();

        // Loop - Request checksums from receiver, cross off from local list as required
        List<Integer> toCheck = new ArrayList<>();
        message.messageAsString(hostname, port, "checksum", response -> {
            Set<Integer> keys = GSON.fromJson(response, new TypeToken<Set<Integer>>() {}.getType());
            // If contains - remove from entryMap as there is no diff
            // If not contains - add to toCheck list
            keys.forEach(key -> {
                if (entryMap.containsKey(key)) {
                    System.out.println(format("Match: {0}", entryMap.get(key).getRelativePath()));
                    entryMap.remove(key);
                } else {
                    toCheck.add(key);
                }
            });
        });
        // Loop end

        // Build up a mapping of the remaining FolderEntry (path -> FolderEntry)
        Map<String, FolderEntry> folderMap = new HashMap<>();
        entryMap.values().forEach(folder -> folderMap.put(folder.getRelativePath(), folder));

        // Loop - For the checksums we do not recognise, request the FolderEntry for these and compare
        //        any that match will need to be diffed against local FolderEntry
        List<String> pathsToRequest = new ArrayList<>();
        List<String> pathsToDelete = new ArrayList<>();
        toCheck.stream().forEach(key -> message.messageAsString(hostname, port, String.valueOf(key), string -> {
            FolderEntry source = GSON.fromJson(string, FolderEntry.class);
            String path = source.getRelativePath();
            FolderEntry target = folderMap.get(path);
            if (target != null) {
                System.out.println(format("Diff: {0}", source.getRelativePath()));
                // Diff the two folder entries
                pathsToRequest.addAll(target.getModifiedOrNewFiles(source));
                pathsToDelete.addAll(target.getDeletedFiles(source));
                // Finally, remove FolderEntry from folderMap
                folderMap.remove(path);
            } else {
                // Copy this new path
                pathsToRequest.addAll(source.getAllFiles());
            }
        }));
        // Loop end

        // For all the paths we need to request, request the path and write to the local file system
        System.out.println(format("Requesting {0} paths", pathsToRequest.size()));
        pathsToRequest.forEach(path -> message.message(hostname, port, path, responseStream -> {
            System.out.println(format("Requesting: {0}", path));
            File file = FolderEntry.getFullPath(root, path);
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            try (FileOutputStream out = new FileOutputStream(file)) {
                copyStream(responseStream, out);
            } catch (IOException ignored) {}
        }));


        // For to remaining FolderEntry in the folderMap, mark them for
        // deletion as we know by this point they have not been found in the
        // source.
        folderMap.values().forEach(entry -> pathsToDelete.addAll(entry.getAllFiles()));

        // Finally, for all files we know we need to remove, remove them.
        System.out.println(format("Deleting {0} paths", pathsToDelete.size()));
        Consumer<String> delete = relative -> FolderEntry.getFullPath(root, relative).delete();
        Consumer<String> print = path -> System.out.println(format("Deleting: {0}", path));
        pathsToDelete.forEach(print.andThen(delete));
    }

    /**
     * Signals to the FolderSync to shutdown.
     */
    public void shutdown() {
        message.shutdown();
    }


}
