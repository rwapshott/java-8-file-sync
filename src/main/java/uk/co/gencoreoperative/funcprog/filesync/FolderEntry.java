package uk.co.gencoreoperative.funcprog.filesync;

import com.google.gson.Gson;

import java.io.File;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

import static java.text.MessageFormat.format;

/**
 * Describes a folder listing with enough detail to compare with another file system.
 *
 * In addition, can include the contents of the files found within the folder.
 */
public class FolderEntry {
    private final static Gson GSON = new Gson();

    final Map<String, Long> files = new HashMap<>();
    final Set<String> folders = new HashSet<>();
    String relativePath;
    private Collection<? extends String> allFiles;

    /**
     * @return The relative will act as a unique identifier.
     */
    public String getRelativePath() {
        return relativePath;
    }

    /**
     * @return checksum for the contents of this object.
     */
    public int checksum() {
        return GSON.toJson(this).hashCode();
    }

    /**
     * @return JSON representation of this {@link FolderEntry}.
     */
    public String toJson() {
        return GSON.toJson(this);
    }

    /**
     * Create a listing of a folder.
     *
     * @param path Path of a folder on the file system.
     * @return Optional indicating that the result could be null if there is a problem
     * accessing the file system or the specific requested folder (read access
     * denied for example)
     *
     * @param path The path of a folder to list.
     * @param root The path of the root folder which the sub path should be below.
     */
    public static Optional<FolderEntry> list(Path root, Path path) {
        FolderEntry entry = new FolderEntry();
        File[] listing = path.toFile().listFiles();
        if (listing == null) {
            return Optional.empty();
        }
        entry.relativePath = getRelative(root, path);
        for (File file : listing) {
            String name = file.getName();
            if (file.isDirectory()) {
                entry.folders.add(name);
            } else {
                entry.files.put(name, file.length());
            }
        }
        return Optional.of(entry);
    }

    public static Optional<FolderEntry> list(Path path) {
        return list(path, path);
    }

    /**
     * Get the relative difference between two paths
     * @param root
     * @param subPath
     * @return
     */
    private static String getRelative(Path root, Path subPath) {
        String rootString = root.toFile().getAbsolutePath();
        String subString = subPath.toFile().getAbsolutePath();
        if (!subString.startsWith(rootString)) {
            throw new IllegalStateException();
        }
        return subString.substring(rootString.length(), subString.length());
    }

    /**
     * @return A non null path to the current working directory.
     */
    public static Path getCWD() {
        String cwd = System.getProperty("user.dir");
        return new File(cwd).toPath();
    }


    public String toString() {
        return format("{0} [{1}]", relativePath, folders.size() + files.size());
    }

    /**
     * Compare two FolderEntry together to identify which files are not the same
     * in both FolderEntry. The entry being provided is considered the source.
     *
     * If a file is missing and needs to be copied it will be included. If a file
     * size differs the file will also need to be copied.
     *
     * This method will not indicate those files which need to be deleted.
     *
     * @param source The entry that is considered the source.
     * @return A list of file paths for files that are non-matching.
     */
    public List<String> getModifiedOrNewFiles(FolderEntry source) {
        if (!getRelativePath().equals(source.getRelativePath())) throw new IllegalStateException();
        return source.files.keySet().stream()
                .filter(name -> !files.keySet().contains(name) || files.get(name) != source.files.get(name))
                .map(name -> getRelativePath() + File.separator + name)
                .map(name -> name.startsWith(File.separator) ? name.substring(File.separator.length()) : name)
                .collect(Collectors.toList());
    }

    /**
     * Compare two FolderEntry together to identify which files should be removed
     * from the target which were not present in the source.
     *
     * @param source Non null entry considered the source of the two {@link FolderEntry}
     * @return A list of file paths for files that should be deleted.
     */
    public List<String> getDeletedFiles(FolderEntry source) {
        if (!getRelativePath().equals(source.getRelativePath())) throw new IllegalStateException();
        return files.keySet().stream()
                .filter(name -> !source.files.containsKey(name))
                .map(name -> getRelativePath() + File.separator + name)
                .map(name -> name.startsWith(File.separator) ? name.substring(File.separator.length()) : name)
                .collect(Collectors.toList());

    }

    public List<String> getAllFiles() {
        return files.keySet().stream()
                .map(name -> getRelativePath() + File.separator + name)
                .map(name -> name.startsWith(File.separator) ? name.substring(File.separator.length()) : name)
                .collect(Collectors.toList());
    }

    public static File getFullPath(File root, String path) {
        return new File(root + File.separator + path);
    }
}
